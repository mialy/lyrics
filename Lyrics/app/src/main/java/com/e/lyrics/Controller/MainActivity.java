package com.e.lyrics.Controller;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.e.lyrics.Model.JavaBean.LyricBean;
import com.e.lyrics.Model.LyricUtils;
import com.e.lyrics.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    //    COMPOSANTS GRAPHIQUES
    private Button mBtnSearch;
    private TextView mTvResult;
    private EditText mEtSinger;
    private EditText mEtTitle;
    private FloatingActionButton mFloatingActionButton;

    //    TOOLS
    private MonAsyncTask monAsyncTask;
    private LinearLayout mLLResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBtnSearch = findViewById(R.id.btnSearch);
        mTvResult = findViewById(R.id.tvResult);
        mEtSinger = findViewById(R.id.et_singer);
        mEtTitle = findViewById(R.id.et_title);
        mBtnSearch = findViewById(R.id.btnSearch);
        mFloatingActionButton = findViewById(R.id.floatingActionButton);
        mLLResult = findViewById(R.id.LL_result);
    }

    /********************************************************************************
     //     * ECOUTEURS ET METHODES
     //     * @param view
     //     */
    public void onClick(View view) {
//        Gestion du lancement de la tache asynchrone
        if(TextUtils.isEmpty(mEtTitle.getText()) || TextUtils.isEmpty(mEtSinger.getText())){
            Toast.makeText(this, "Merci de remplir tous les champs", Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this, "Recherche en cours", Toast.LENGTH_SHORT).show();
            monAsyncTask = new MonAsyncTask(mEtSinger.getText().toString(), mEtTitle.getText().toString());
            monAsyncTask.execute();
        }

    }

    public void onClickReturn(View view) {
        mTvResult.setText("");
        mEtSinger.setText("");
        mEtTitle.setText("");
        mLLResult.setVisibility(View.GONE);
    }

    /********************************************************************************
     * ASYNTASK en classe interne POUR LES REQUETES
     */
    public class MonAsyncTask extends AsyncTask {
        LyricBean lyricFound;
        Exception exception;
        String singer;
        String title;

        public MonAsyncTask(String s, String t) {
            this.singer = s;
            this.title = t;
        }


        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                lyricFound = LyricUtils.getLyricFromWeb(singer, title);

            } catch (Exception e) {
                e.printStackTrace();
                exception = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            mLLResult.setVisibility(View.VISIBLE);

            //On peut utiliser les composants graphiques car retour sur le thread principal
            if (exception != null) {
                mTvResult.setText( "\n Merci de vérifier l' orthographe \n" +exception.getMessage() );
                Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();

            } else {
                // pour le textView
//                String aAfficher = "";
//                for (CityBean cityBean : listeVilleResult) {
//                    aAfficher += cityBean.getCp() + " \t: \t " + cityBean.getVille() + " \n";
//                }

                Toast.makeText(MainActivity.this, "Trouvé", Toast.LENGTH_SHORT).show();
                mTvResult.setText(lyricFound.getLyrics());
            }
        }
    }

}

