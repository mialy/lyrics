package com.e.lyrics.View;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.e.lyrics.R;

public class FindLyricActivity extends AppCompatActivity {

//    Composants graphiques
    private ImageView mImageView2;
    private ProgressBar mProgressBar;
    private TextView mVousEtes;
    private EditText mEtEmail;
    private Button mBtnCompteExistant;

    //    Attributs tools
//    private MonAsyncTask monAsyncTask;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_lyric);
        mImageView2 = findViewById(R.id.imageView2);
        mProgressBar = findViewById(R.id.progressBar);
        mVousEtes = findViewById(R.id.tv_rechercher);
        mEtEmail = findViewById(R.id.et_email);
        mBtnCompteExistant = findViewById(R.id.btnCompteExistant);
    }

    public void onClick(View view) {
    }

//    /********************************************************************************
//     * ECOUTEURS ET METHODES
//     * @param view
//     */
//    public void onClick(View view) {
//        Toast.makeText(this, "Recherche en cours", Toast.LENGTH_SHORT).show();
////        monAsyncTask = new MonAsyncTask(mEtRechercher.getText().toString());
//        monAsyncTask.execute();
//    }
//
//    /********************************************************************************
//     * ASYNTASK en classe interne POUR LES REQUETES
//     */
//    public class MonAsyncTask extends AsyncTask {
//        LyricBean lyricFound;
//        Exception exception;
////        String cp;
//
//        //        monAT constructor
////        public MonAsyncTask(String cp) {
////            this.cp = cp;
////        }
//
//        @Override
//        protected Object doInBackground(Object[] objects) {
//            try {
//                lyricFound = LyricUtils.getLyricFromWeb("","");
////                lyricFound = VilleSearchUtils.getCities(cp);
//            } catch (Exception e) {
//                e.printStackTrace();
//                exception = e;
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Object o) {
//            super.onPostExecute(o);
//
//            //On peut utiliser les composants graphiques car retour sur le thread principal
//            if (exception != null) {
//                mVousEtes.setText(exception.getMessage());
//                Toast.makeText(FindLyricActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
//
//            } else {
//
//                // pour le textView
//                String aAfficher = "";
//                for (CityBean cityBean : listeVilleResult) {
//                    aAfficher += cityBean.getCp() + " \t: \t " + cityBean.getVille() + " \n";
//                }
//                Toast.makeText(CPVilleActivity.this, "Trouvé", Toast.LENGTH_SHORT).show();
//                mResultat.setText(aAfficher);
//            }
//        }
//    }
//
// }

}
