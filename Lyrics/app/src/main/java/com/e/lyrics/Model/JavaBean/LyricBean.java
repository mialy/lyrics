package com.e.lyrics.Model.JavaBean;

public class LyricBean {

    private String lyrics;
//    Contstructor
    public LyricBean(String lyrics) {
        this.lyrics = lyrics;
    }

    public LyricBean() {

    }

    //    Getter setter
    public String getLyrics() {
            return lyrics;
        }

        public void setLyrics(String lyrics) {

            this.lyrics = lyrics;
        }

        public LyricBean withLyrics(String lyrics) {
            this.lyrics = lyrics;
            return this;
        }


}
