package com.e.lyrics.Model.JavaBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorBean {

        @SerializedName("error")
        @Expose
        private String errorMessage;

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public ErrorBean withError(String error) {
            this.errorMessage = error;
            return this;
        }


}
