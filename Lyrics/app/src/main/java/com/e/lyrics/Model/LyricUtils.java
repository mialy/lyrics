package com.e.lyrics.Model;

import com.e.lyrics.Model.JavaBean.LyricBean;
import com.e.lyrics.Model.JavaBean.ResultBean;
import com.google.gson.Gson;

public class LyricUtils {

    public static String singer ;
    public static String title ;
    public static final String URL_Prefix = "https://api.lyrics.ovh/v1/";
//    private static  String FULL_URL = URL_Prefix + singer +"/"+ title;


    public static LyricBean getLyricFromWeb(String singerSearched, String titleSearched) throws Exception {
//       Test
//       FULL_URL = "https://api.lyrics.ovh/v1/Coldplay/Adventure%20of%20a%20Lifetime";

//  Construction de l'URL
        String FULL_URL = URL_Prefix + singerSearched +"/"+ titleSearched;
        String jsonResponse = OkHttpUtils.sendGetOkHttpRequest(FULL_URL);

//  Parse avec gson
        Gson gson = new Gson();
        LyricBean lyricBeanResult = gson.fromJson(jsonResponse, LyricBean.class);


////  Erreur
//        if (resultBean.getErrorResult() != null) {
//            throw new Exception(resultBean.getErrorResult().getErrorMessage());
//        } else {
            return lyricBeanResult;
//        }
    }
}


